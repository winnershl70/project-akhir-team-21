@extends('layout.master')

@section('judul')
 Halam Berita
@endsection

@section('content')
@auth
<a href="/berita/create" class="btn btn-primary  my-2">tambah berita</a>
@endauth
<div class="row">
    @forelse ($berita as $item)
    <div class="col-4">
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="{{asset('gambar/'.$item->gambar)}}" alt="Card image cap">
            <div class="card-body">
              <h2>{{$item->judul}}</h2>
              <p class="card-text">{{$item->isi}}</p>
             
              <form action="/berita/{{$item->id}}" method="POST">
                @csrf
                @method('DELETE') 
                <a href="/berita/{{$item->id}}" class="btn btn-info btn-sm">detail</a>
                @auth
                <a href="/berita/{{$item->id}}/edit" class="btn btn-success btn-sm">edit</a>
                <input type="submit" class="btn btn-danger btn-sm" value="delete">
                @endauth
             </form>
            </div>
          </div>
    </div>
    @empty
        <h2>data kosong</h2>
    @endforelse
    
</div>
@endsection