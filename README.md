## About Project

Ini adalah project dari Bootcamp **PKS Digital School**, project ini dikembangkan menggunakan framework Laravel versi 6.x.
<br>
Project ini dikerjakan secara team dan tiap team berisikan 3 anggota. Berikut nama-nama anggota team kami :
- Dzaki Sholahuddin - [@Dzaki S](https://t.me/Dzaki).
- Abdullah Faqih Rachmat - [@faqiihr](https://t.me/faqiihr).
- Muhamad Taufiq Riza - [@taufiqriza628](https://t.me/taufiqriza628).


## Tema Project

Dalam project ini, kami membuat **Sistem Informasi Berita (SIB)**. Sistem ini memiliki 3 fungsi utama, yaitu :
1. Mengelola data Berita
2. Mengelola data Artikel
3. Mengelola data sirkulasi


## ERD 
<img src="gambarErd.png">

## Link Video

Link demo aplikasi :
Link Video aplikasi :

Installation Project

Buka cmd lalu ketikkan
Cloning repository : git clone https://gitlab.com/winnershl70/project-akhir-team-21.git

Masuk ke directory project : cd project-akhir-team-21

composer install
Buat file .env, isinya diambil dari .env.example

php artisan key:generate
php artisan migrate
php artisan serve


License
The Laravel framework is open-sourced software licensed under the MIT license.
