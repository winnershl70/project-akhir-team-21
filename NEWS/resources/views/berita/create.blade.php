@extends('layout.master')

@section('judul')
    Form Publish Berita
@endsection

@section('content')

@push('script')
<script>
  tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script>
   // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
    $('.js-example-basic-single').select2();
});
  </script>
@endpush
@push('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush


<div>
    <h2>Tambah Data</h2>
        <form action="/berita" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="judul">judul</label>
                <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan judul">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="gambar">gambar</label>
                <input type="file" class="form-control" name="gambar" id="gambar" placeholder="Masukkan gambar" >
                @error('gambar')
                    <div class="alert alert-danger" >
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="isi">isi berita</label>
                <textarea name="isi" id="isi" placeholder="Masukkan isi" ></textarea>
                @error('isi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
           
            <div class="form-group">
                <label >kategori</label><br>
                <select name="kategori_id"  class="js-example-basic-single"   style="width: 100%" >
                    <option value="">---pilih kategori---</option>
                    @foreach ($kategori as $item)
                        <option value="{{$item->id}}">{{$item ->nama}}</option>
                    @endforeach  
                </select>
                @error('kategori_id')
                    <div class="alert alert-danger" >
                        {{ $message }}
                    </div>
                @enderror
            </div>

           
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>

@endsection