<!doctype html>
<html lang="en">

  <head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="megakit,business,company,agency,multipurpose,modern,bootstrap4">
  
  <meta name="author" content="themefisher.com">
  {{-- <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
    
        <title>{{ config('app.name', 'Laravel') }}</title>
    
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    
        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
  <title>Berita Terkini</title>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> 
  <!-- bootstrap.min css -->
  <link rel="stylesheet" href="{{asset('megakit/plugins/bootstrap/css/bootstrap.min.css')}}">
  <!-- Icon Font Css -->
  <link rel="stylesheet" href="{{asset('megakit/plugins/themify/css/themify-icons.css')}}">
  <link rel="stylesheet" href="{{asset('megakit/plugins/fontawesome/css/all.css')}}">
  <link rel="stylesheet" href="{{asset('megakit/plugins/magnific-popup/dist/magnific-popup.css')}}">
  <!-- Owl Carousel CSS -->
  <link rel="stylesheet" href="{{asset('megakit/plugins/slick-carousel/slick/slick.css')}}">
  <link rel="stylesheet" href="{{asset('megakit/plugins/slick-carousel/slick/slick-theme.css')}}">

  <!-- Main Stylesheet -->
  <link rel="stylesheet" href="{{asset('megakit/css/style.css')}}">
  @stack('style')
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.0.min.js"></script>
  <script src="https://cdn.tiny.cloud/1/gpjwsbhz8c6ihq0c6ztqt2upxgo8dfzc0qm16c90dl9qsgva/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
</head>

<body>
  @include('sweetalert::alert')
    <!-- Navbar -->
    @include('partial.nav')
    <!-- /.navbar -->


  <div class="main-wrapper ">
	    <div class="container">
        <div class="row mt-5">
          <div class="col-lg-8">
            <div class="section-title">
              <h2 class="mt-3 content-title">@yield('judul')</h2>
              <span class="h6 text-color ">Berita Aktual dan Terkini</span>
            </div>
          </div>
        </div>
        @yield('content')
      </div>
  </div>

  <!-- Navbar -->
  @include('partial.footer')
  
  
    <!-- 
    Essential Scripts
    =====================================-->

    
    <!-- Main jQuery -->
    <script src="{{asset('admin/plugins/jquery/jquery.js')}}"></script>
    <script src="{{asset('admin/js/contact.js')}}"></script>
    <!-- Bootstrap 4.3.1 -->
    <script src="{{asset('admin/plugins/bootstrap/js/popper.js')}}"></script>
    <script src="{{asset('admin/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
   <!--  Magnific Popup-->
    <script src="{{asset('admin/plugins/magnific-popup/dist/jquery.magnific-popup.min.js')}}"></script>
    <!-- Slick Slider -->
    <script src="{{asset('admin/plugins/slick-carousel/slick/slick.min.js')}}"></script>
    <!-- Counterup -->
    <script src="{{asset('admin/plugins/counterup/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('admin/plugins/counterup/jquery.counterup.min.js')}}"></script>

    <!-- Google Map -->
    <script src="{{asset('admin/plugins/google-map/map.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkeLMlsiwzp6b3Gnaxd86lvakimwGA6UA&callback=initMap"></script>    
    
    <script src="{{asset('admin/js/script.js')}}"></script>

  @stack('script')      
</body>
</html>
   