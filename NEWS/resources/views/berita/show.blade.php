@extends('layout.master')

@section('judul')
    Detail Berita
@endsection

@section('content')


<div class="col-md-7 col-sm-12 mb-5 bg-white p-0">
    <div class="p-4">
        <h2>{{ $berita->judul }}</h2>
        <p class="mt-5">{{ $berita->isi }}</p>
        <img src="{{ asset('gambar/'.$berita->gambar) }}" width="1000">
{{$berita->isi}}
    </div>
</div>
@forelse ($berita ->komentar as $item)
    <div class="card" >
    <div class="card-body">
        <h5 class="card-title">{{$item->users->name}}</h5>
        <p class="card-text">{{$item->isi}}</p>
    </div>
    </div>
@empty
    <h3>tidak ada komentar</h3><br><br>
@endforelse
<form action="/komentar" method="POST" >
    @csrf
    <div class="form-group">
        <input type="hidden" value="{{$berita->id}}" name="berita_id">
        <textarea name="isi" id="isi" placeholder="Masukkan komentar" ></textarea>
        @error('isi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror <br>
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>
@endsection