<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Berita;
use File;

use RealRashid\SweetAlert\Facades\Alert;
class BeritaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['show','store','index']);

        // $this->middleware('log')->only('index');

        // $this->middleware('subscribed')->except('store');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $berita = Berita::all();
        return view('berita.index', compact("berita"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = DB::table('kategori')->get();
        return view('berita.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
            'kategori_id' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg'

        ]);
        $namaGambar = time().'.'.$request->gambar->extension();  
     
        $request->gambar->move(public_path('gambar'), $namaGambar);

        
        $berita = new Berita;

        $berita->judul = $request->judul;
        $berita->isi = $request->isi;   
        $berita->kategori_id = $request->kategori_id;
        $berita->gambar =  $namaGambar;

        $berita->save();
        Alert::success('Berhasil', 'Berhasil Tambah Data');
        return redirect('/berita');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kategori = DB::table('kategori')->get();
        $berita = Berita::findOrFail($id);
        return view('berita.show', compact('berita','kategori'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = DB::table('kategori')->get();
        $berita = Berita::findOrFail($id);
        return view('berita.edit', compact('berita','kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
            'kategori_id' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg'

        ]);

        if ($request->has('gambar')) {
            $berita = Berita::find($id);
            $path = 'gambar/';
            File::delete($path . $berita->gambar);
            $namaGambar = time().'.'.$request->gambar->extension();  
     
            $request->gambar->move(public_path('gambar'), $namaGambar);

            $berita->judul = $request->judul;
            $berita->isi = $request->isi;   
            $berita->kategori_id = $request->kategori_id;
            $berita->gambar =  $namaGambar;

            $berita->save();
            return redirect('/berita');
        } else {
            $berita = Berita::find($id);
            $berita->judul = $request->judul;
            $berita->isi = $request->isi;   
            $berita->kategori_id = $request->kategori_id;

            $berita->save();
            Alert::success('Berhasil', 'Berhasil Edit Data');
            return redirect('/berita');
        }
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = Berita::find($id);
        $path = "gambar/";
        File::delete($path . $berita->gambar);

        $berita->delete();
        Alert::success('Berhasil', 'Berhasil Hapus Data');
        return redirect('/berita');
    }
}
