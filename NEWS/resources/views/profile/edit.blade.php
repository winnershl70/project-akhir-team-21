@extends('layouts.master');

@section('content')

@auth
<div>
        <h2>Edit Profilr {{$profile->id}}</h2>
        <form action="/profile/{{$profile->id}}" method="PUT">
            @csrf
            {{-- @method('patch') --}}

            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="text" class="form-control" name="umur" value="{{$profile->umur}}" id="title" placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea class="form-control" name="alamat"  value="{{$profile->alamat}}"  id="alamat" placeholder="Masukkan Alamat"></textarea>
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="bio">Biodata</label>
                <textarea class="form-control" name="bio"  value="{{$profile->bio}}"  id="bio" placeholder="Biodata"></textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
    @endauth

@endsection