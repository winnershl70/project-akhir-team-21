<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table = 'berita';

    protected $fillable = ['judul','isi','gambar','kategori_id'];

    public function komentar()
    {
        return $this->hasMany('App\Komentar');
    }
}
