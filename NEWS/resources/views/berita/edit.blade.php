@extends('layout.master')

@section('judul')
   edit berita
@endsection

@section('content')

@push('script')
<script src="https://cdn.tiny.cloud/1/gpjwsbhz8c6ihq0c6ztqt2upxgo8dfzc0qm16c90dl9qsgva/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush   
<div>
    <h2>Edit Data</h2>
        <form action="/berita/{{$berita->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="judul">judul</label>
                <input type="text" class="form-control" name="judul" value="{{$berita->judul}}"  id="judul" placeholder="Masukkan judul">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="gambar">gambar</label>
                <input type="file" class="form-control" name="gambar"  id="gambar" placeholder="Masukkan gambar" >
                @error('gambar')
                    <div class="alert alert-danger" >
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="isi">isi berita</label>
                <textarea class="form-control" name="isi" id="isi" value="{{$berita->isi}}"  placeholder="Masukkan isi" cols="30" rows="10"></textarea>
                @error('isi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
           
            <div class="form-group">
                <label for="kategori_id">kategori</label>
                <select name="kategori_id" id="kategori_id"   class="form-control" >
                    <option value="">---pilih kategori---</option>
                    @foreach ($kategori as $item)
                    @if ( $item->id === $berita->kategori_id)
                            
                    <option value="{{$item->id}}" selected>{{$item ->nama}}</option>
                    @else
                        
                    <option value="{{$item->id}}">{{$item ->nama}}</option>
                    @endif
                    @endforeach  
                </select>
                @error('kategori_id')
                    <div class="alert alert-danger" >
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">edit</button>
        </form>
</div>

@endsection