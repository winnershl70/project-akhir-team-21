<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layout.master');
// });

Route::get('/','IndexController@index' );

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//kategori
Route::resource('profile','ProfileController');
Route::resource('kategori','KategoriController');
Auth::routes();
Route::resource('berita','BeritaController');
Auth::routes();
Route::resource('komentar','KomentarController')->only(['store']);